export const environment = {
  production: true,
  apiUrl: 'https://apiwww.cinesoft.cu/v1/',
  imageUrl: 'https://apiwww.cinesoft.cu/v1/',
  defaultLanguage: 'es',
  version: '0.1.1',
};
